function fish_prompt
    set -g primary (set_color yellow)
    set -g secondary (set_color cyan)
    set -g tertiary (set_color blue)
    set -g alert (set_color red)
    set -g normal (set_color normal)

    function seg_pwd
        set -l sep /
        set -l info (pwd_info $sep)

        if pwd_is_home
            echo -ns $secondary '~'

            if test -n $info[1]
                echo -n $sep
            end
        else
            echo -ns $secondary '/'
        end

        if test -n $info[2]  # truncated path
            echo -ns $secondary $info[2] $sep $normal
        end

        if test -n $info[1]  # repo/curdir
            echo -ns $primary $info[1]
        end

        if test -n $info[3] # repo subdir
            echo -ns $tertiary $sep $info[3] $normal
        end

        echo -n ' '
    end

    function seg_venv
        if test -n "$VIRTUAL_ENV"
            echo -ns $primary '🐍' $normal ' '
        end
    end

    function seg_git
        if not git_is_repo
            return
        end

        set -l branch (git_branch_name)

        echo -n '['

        set -l ahead (git_ahead)
        if test -n $ahead
            echo -ns $secondary $ahead $normal
        end

        echo -ns $primary $branch $normal

        if git_is_dirty
            # changes not staged for commit
            echo -n '*'
        else if git_is_staged
            # changes staged for commit
            echo -n '+'
        else
            # no changes
            set no_changes
        end

        set -l untracked (git_untracked_files)
        if test $untracked -ne 0
            if set -q no_changes
                echo -n ' '
            end
            echo -ns $alert $untracked $normal
        end

        echo -n '] '
    end

    seg_venv
    seg_git
    seg_pwd

    set -e primary
    set -e secondary
    set -e alert
    set -e normal
end
