#! /bin/bash
source setup/_common.bash

setup_linux() {
    if [ ! -z "$(which apt)" ]; then
        info "entering stage 0"
        setup/stage0-deb.bash
    else
        echo "error: unknown linux distro"
        exit 1
    fi
}

finish_repo() {
    git submodule init
    git submodule update
    local blackenpath="emacs/.emacs.d/private/spaceblacken"
    if [ ! -d "$blackenpath" ] ; then
        git clone --quiet https://github.com/BackwardSpy/spaceblacken.git "$blackenpath"
    fi
}

echo "bootstrap"

info "finishing repo"
finish_repo

info "checking system configuration"
case "$(uname)" in
    Linux)
        info "running in linux mode"
        setup_linux
        ;;
    *)
        echo "error: unsupported platform $PLATFORM"
        exit 1
        ;;
esac
