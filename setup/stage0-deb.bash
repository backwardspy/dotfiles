#! /bin/bash
source setup/_common.bash

echo "stage 0 running in deb mode"

info "updating apt"
sudo apt-get -qq update

info "installing python3"
sudo apt-get -qq install python3 python3-pip

info "setting up environment for stage 1"
venv=$(mktemp -d)
python3 -m venv $venv
$venv/bin/pip3 -q install plumbum

info "entering stage 1"
$venv/bin/python3 setup/stage1.py deb
