#! /bin/bash
set -eu -o pipefail

info() {
    echo " - $1"
}
